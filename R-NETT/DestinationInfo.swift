//
//  DestinationInfo.swift
//  R-NETT
//
//  Created by Mac103 on 10/14/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import Foundation
import CoreData
@objc(DestinationInfo)
class DestinationInfo: NSManagedObject {

    @NSManaged var destinationId: NSNumber
    @NSManaged var destinationName: String
    @NSManaged var address1: String
    @NSManaged var address2: String
    @NSManaged var city: String
    @NSManaged var state: String
    @NSManaged var zip: String
    @NSManaged var phoneNo: String
    @NSManaged var date: NSDate

}
