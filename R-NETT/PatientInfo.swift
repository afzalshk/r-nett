//
//  PatientInfo.swift
//  R-NETT
//
//  Created by Mac103 on 10/14/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import Foundation
import CoreData

@objc(PatientInfo)
class PatientInfo: NSManagedObject {

    @NSManaged var patientId: NSNumber
    @NSManaged var firstName: String
    @NSManaged var lastName: String
    @NSManaged var insurance: String
    @NSManaged var birthDate: String
    @NSManaged var memberId: String
    @NSManaged var roomNo: String
    @NSManaged var date: NSDate

}
