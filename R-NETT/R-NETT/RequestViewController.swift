//
//  RequestViewController.swift
//  R-NETT
//
//  Created by Mac103 on 10/13/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import UIKit
import MapKit
import CoreData

class RequestViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var reasonView: UIView!
    @IBOutlet weak var destinationView: UIView!
    @IBOutlet weak var patientInfoView: UIView!
    @IBOutlet weak var pickupView: UIView!
    @IBOutlet weak var btnTaxi: UIButton!
    @IBOutlet weak var btnVan: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnAddMore: UIButton!
    
    @IBOutlet weak var txtPickUpName: UITextField!
    @IBOutlet weak var txtPickUpAddress: UITextField!
    @IBOutlet weak var txtPickUpCity: UITextField!
    @IBOutlet weak var txtPickUpState: UITextField!
    @IBOutlet weak var txtPickUpZip: UITextField!
    @IBOutlet weak var txtPickUpPhoneNo: UITextField!
    @IBOutlet weak var txtRequesterName: UITextField!
    @IBOutlet weak var txtRequestDate: UITextField!
    @IBOutlet weak var txtPickUpEAT: UITextField!
    
    @IBOutlet weak var txtPatientLastName: UITextField!
    @IBOutlet weak var txtPatientFirstName: UITextField!
    @IBOutlet weak var txtPatientInsurance: UITextField!
    @IBOutlet weak var txtPatientMemberId: UITextField!
    @IBOutlet weak var txtPatientDOB: UITextField!
    @IBOutlet weak var txtPatientRoom: UITextField!
    
    @IBOutlet weak var txtDestName: UITextField!
    @IBOutlet weak var txtDestAddress1: UITextField!
    @IBOutlet weak var txtDestAddress2: UITextField!
    @IBOutlet weak var txtDestCity: UITextField!
    @IBOutlet weak var txtDestState: UITextField!
    @IBOutlet weak var txtDestZip: UITextField!
    @IBOutlet weak var txtDestPhoneNo: UITextField!
    
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var places: CLPlacemark?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUIView()
        getPlaces(places!)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        
        return true
    }
    
    @IBAction func btnSaveAction(sender: UIButton) {
        saveRequestData()
    }
    
    func saveRequestData()
    {
        addPickUpInfo()
        addPatientInfo()
        addDestinationInfo()
        //addReasonInfo()
        let requestDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RequestDetailVC") as! RequestDetailViewController
        self.navigationController?.pushViewController(requestDetailViewController, animated: true)
    }
    
    func addPickUpInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("PickUpInfo", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(txtPickUpName.text, forKey: "facilityName")
        myMO.setValue(txtPickUpAddress.text, forKey: "address")
        myMO.setValue(txtPickUpCity.text, forKey: "city")
        myMO.setValue(txtPickUpState.text, forKey: "state")
        myMO.setValue(txtPickUpZip.text, forKey: "zip")
        myMO.setValue(txtPickUpPhoneNo.text, forKey: "phoneNo")
        myMO.setValue(txtRequesterName.text, forKey: "requesterName")
        myMO.setValue(txtRequestDate.text, forKey: "requestDate")
        myMO.setValue(txtPickUpEAT.text, forKey: "estimatedPickTime")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }

    }
    func addPatientInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("PatientInfo", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(txtPatientFirstName.text, forKey: "firstName")
        myMO.setValue(txtPatientLastName.text, forKey: "lastName")
        myMO.setValue(txtPatientInsurance.text, forKey: "insurance")
        myMO.setValue(txtPatientDOB.text, forKey: "birthDate")
        myMO.setValue(txtPatientMemberId.text, forKey: "memberId")
        myMO.setValue(txtPatientRoom.text, forKey: "roomNo")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
    }
    
    func addDestinationInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("DestinationInfo", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(txtDestName.text, forKey: "destinationName")
        myMO.setValue(txtDestAddress1.text, forKey: "address1")
        myMO.setValue(txtDestAddress2.text, forKey: "address2")
        myMO.setValue(txtDestCity.text, forKey: "city")
        myMO.setValue(txtDestState.text, forKey: "state")
        myMO.setValue(txtDestZip.text, forKey: "zip")
        myMO.setValue(txtDestPhoneNo.text, forKey: "phoneNo")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
    }
    
    func addReasonInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("Reasons", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(txtPickUpAddress.text, forKey: "reasonDesc")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
    
    }
    
    func getPlaces(places: CLPlacemark)
    {
        println(places.addressDictionary)
        if let locationName = places.addressDictionary["Name"] as? NSString {
            txtPickUpName?.text = locationName as! String
            //println(locationName)
        }
        
        // Street address
        if let street = places.addressDictionary["Thoroughfare"] as? NSString {
            txtPickUpAddress?.text = street as String
            //println(street)
        }
        
        // City
        if let city = places.addressDictionary["City"] as? NSString {
           txtPickUpCity?.text = city as String
            //println(city)
        }
        
        // Zip code
        if let zip = places.addressDictionary["ZIP"] as? NSString {
           txtPickUpZip?.text = zip as String
            //println(zip)
        }
        if let state = places.addressDictionary["State"] as? NSString {
            txtPickUpState?.text = state as String
            //println(state)
        }
        
        // Country
        if let country = places.addressDictionary["Country"] as? NSString {
            //
        }
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter.timeZone = NSTimeZone()
         let localDate = dateFormatter.stringFromDate(date)

            txtRequestDate.text = localDate
    }

    func setUIView()
    {
        pickupView.layer.cornerRadius = 4
        pickupView.clipsToBounds = true
        patientInfoView.layer.cornerRadius = 4
        patientInfoView.clipsToBounds = true
        destinationView.layer.cornerRadius = 4
        destinationView.clipsToBounds = true
        reasonView.layer.cornerRadius = 4
        reasonView.clipsToBounds = true
        btnTaxi.layer.cornerRadius = 4
        btnTaxi.clipsToBounds = true
        btnVan.layer.cornerRadius = 4
        btnVan.clipsToBounds = true
        btnEdit.layer.cornerRadius = 4
        btnEdit.clipsToBounds = true
        btnAddMore.layer.cornerRadius = 4
        btnAddMore.clipsToBounds = true
        btnSave.layer.cornerRadius = 4
        btnSave.clipsToBounds = true
        mainScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 2150)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnBackAction(sender: UIButton) {
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // Text Field Delegates
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        //
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.resignFirstResponder()
        
    }
}
