//
//  MakeRequestViewController.swift
//  R-NETT
//
//  Created by Irfan Malik on 10/20/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import UIKit
import CoreData

class MakeRequestViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var main_View: UIView!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet weak var container_View: UIView!
    
    @IBOutlet weak var txtPickUpName: UITextField!
    @IBOutlet weak var txtPickUpAddress: UITextField!
    @IBOutlet weak var txtPickUpCity: UITextField!
    @IBOutlet weak var txtPickUpState: UITextField!
    @IBOutlet weak var txtPickUpZip: UITextField!
    @IBOutlet weak var txtPickUpPhoneNo: UITextField!
    @IBOutlet weak var txtRequesterName: UITextField!
    @IBOutlet weak var txtRequestDate: UITextField!
    @IBOutlet weak var txtPickUpEAT: UITextField!
    
    @IBOutlet weak var txtPatientLastName: UITextField!
    @IBOutlet weak var txtPatientFirstName: UITextField!
    @IBOutlet weak var txtPatientInsurance: UITextField!
    @IBOutlet weak var txtPatientMemberId: UITextField!
    @IBOutlet weak var txtPatientDOB: UITextField!
    @IBOutlet weak var txtPatientRoom: UITextField!
    
    @IBOutlet weak var txtDestName: UITextField!
    @IBOutlet weak var txtDestAddress1: UITextField!
    @IBOutlet weak var txtDestAddress2: UITextField!
    @IBOutlet weak var txtDestCity: UITextField!
    @IBOutlet weak var txtDestState: UITextField!
    @IBOutlet weak var txtDestZip: UITextField!
    @IBOutlet weak var txtDestPhoneNo: UITextField!
    @IBOutlet weak var btnReasons: UIButton!
    @IBOutlet weak var tblReasons: UITableView!
    @IBOutlet weak var lblReasons: UILabel!

    @IBOutlet weak var btnOxford: UIButton!
    @IBOutlet weak var btnUited: UIButton!
    @IBOutlet weak var btnBlueCross: UIButton!
    @IBOutlet weak var btnMedical: UIButton!
    @IBOutlet weak var btnMedicare: UIButton!
    @IBOutlet weak var btnLACaries: UIButton!
    @IBOutlet weak var btnPrivate: UIButton!
    
    @IBOutlet weak var btnGurney: UIButton!
    @IBOutlet weak var btnBLS: UIButton!
    @IBOutlet weak var btnALS: UIButton!
    @IBOutlet weak var btnCCT: UIButton!
    
    @IBOutlet weak var lblRequestDate: UILabel!
    @IBOutlet weak var lblRequestHours: UILabel!
    @IBOutlet weak var lblRequestMins: UILabel!
    @IBOutlet weak var lblRequestAmPm: UILabel!
    
    @IBOutlet weak var patientBirthDatePicker: UIDatePicker!
    @IBOutlet weak var lblpatientBirthMonth: UILabel!
    @IBOutlet weak var lblpatientBirthDay: UILabel!
    @IBOutlet weak var lblpatientBirthYear: UILabel!
    @IBOutlet weak var txtViewComments: UITextView!
    
    @IBOutlet weak var btnSwitch: UISwitch!
    
    let tapGes = UITapGestureRecognizer()
    var selectetServiceType:String?
    var selectetInsurance:String?
    var pickUpName: String?
    var PickUpAddress: String?
    var PickUpCity: String?
    var PickUpState: String?
    var PickUpZip: String?
    var isSwitch:Bool = false
    
    let reasonsArray: NSArray = ["Doctor's Appt","Physical Therapy","Doctor's Appt","Physical Therapy"]
    let serviceTypeArray: NSArray = ["Gurney","BLS","ALS","CCT"]
    let insuranceArray: NSArray = ["Oxford","Uited","Blue Cross Blue Shields","Medical","Medicare","LA Caries","Private"]
   let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        btnSwitch.on = isSwitch
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenHeight = screenSize.height
        if(screenHeight == 568)
        {
            self.scroll.frame = CGRectMake(0,0,320,505)
            self.scroll.contentSize = CGSizeMake(320,2020);
        }
        else if (screenHeight == 667)
        {
            self.scroll.frame = CGRectMake(0,0,375,570)
            self.scroll.contentSize = CGSizeMake(375,2020);
        }
        else if (screenHeight == 736)
        {
          self.scroll.frame = CGRectMake(0,0,414,570)
          self.scroll.contentSize = CGSizeMake(414,2020);
        }
        
        
        self.container_View.addSubview(self.scroll)
        self.main_View.layer.cornerRadius =  8.0
        self.main_View.clipsToBounds = true
        self.main_View.layer.borderWidth = 3.0;
        self.main_View.layer.borderColor = UIColor(red: 243/255.0, green: 241/255.0, blue: 242/255.0, alpha: 1.0).CGColor
        tblReasons.tableFooterView = UIView(frame: CGRectZero)
        btnGurney.selected = true
        selectetServiceType = "Gurney"
        btnOxford.selected = true
        selectetInsurance = "Oxford"
        initialSetUp()
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    func initialSetUp() {
        tapGes.addTarget(self, action: "gotoTapped")
        self.view.addGestureRecognizer(tapGes)
    
        
        txtPickUpName.text = pickUpName
        patientBirthDatePicker.backgroundColor = UIColor(red: 231.0/256.0, green: 231.0/256.0, blue: 231.0/256.0, alpha: 1.0)
        patientBirthDatePicker.setValue(UIColor(red: 66.0/256.0, green: 66.0/256.0, blue: 66.0/256.0, alpha: 1.0), forKeyPath: "textColor")
        patientBirthDatePicker.setValue(1.0, forKeyPath: "alpha")
        
        patientBirthDatePicker.setDate(NSDate(), animated: true)
        patientBirthDatePicker.maximumDate = NSDate()
        patientBirthDatePicker.datePickerMode = UIDatePickerMode.Date
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        let localDate = dateFormatter.stringFromDate(date)
        lblRequestDate.text = localDate
        
        let calendar = NSCalendar.currentCalendar()
        let comp = calendar.components((.CalendarUnitHour | .CalendarUnitMinute), fromDate: date)
        let hour = comp.hour
        let minute = comp.minute
        lblRequestHours.text = "\(hour)"
        lblRequestMins.text = "\(minute)"
        
    }
    
    func gotoTapped()
    {
        if !patientBirthDatePicker.hidden {
            patientBirthDatePicker.hidden = true
        }
        keyBoardHide()
    }
    
    func keyBoardHide()
    {
        txtViewComments.resignFirstResponder()
        txtPickUpName.resignFirstResponder()
        //txtPickUpAddress.resignFirstResponder()
        //txtPickUpCity.resignFirstResponder()
        //txtPickUpState.resignFirstResponder()
        //txtPickUpZip.resignFirstResponder()
        //txtPickUpPhoneNo.resignFirstResponder()
        txtRequesterName.resignFirstResponder()
        //txtRequestDate.resignFirstResponder()
        //txtPickUpEAT.resignFirstResponder()
        
        txtPatientLastName.resignFirstResponder()
        txtPatientFirstName.resignFirstResponder()
        //txtPatientInsurance.resignFirstResponder()
        txtPatientMemberId.resignFirstResponder()
        //txtPatientDOB.resignFirstResponder()
        txtPatientRoom.resignFirstResponder()
        
        txtDestName.resignFirstResponder()
        //txtDestAddress1.resignFirstResponder()
        //txtDestAddress2.resignFirstResponder()
        //txtDestCity.resignFirstResponder()
        //txtDestState.resignFirstResponder()
        //txtDestZip.resignFirstResponder()
        //txtDestPhoneNo.resignFirstResponder()
    }
    
    
    @IBAction func datePickerChanged(sender: AnyObject) {
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        var strDate = dateFormatter.stringFromDate(patientBirthDatePicker.date)
        
        var chosenDate = patientBirthDatePicker.date
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        let components = myCalendar!.components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay, fromDate: chosenDate)
        var (year, month, date) = (components.year, components.month, components.day)
        
        let dateFormate: NSDateFormatter = NSDateFormatter()
        let months = dateFormate.monthSymbols
        let monthSymbol = months[month-1] as! String
        
        
         lblpatientBirthMonth.text = "\(monthSymbol)"
         lblpatientBirthDay.text = "\(date)"
         lblpatientBirthYear.text = "\(year)"
    }
    

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reasonsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        cell.textLabel?.text = reasonsArray[indexPath.row] as? String
        cell.textLabel?.textColor = UIColor(red: 66.0/256.0, green: 66.0/256.0, blue: 66.0/256.0, alpha: 1.0)
        cell.textLabel?.font = UIFont(name: "Helvetica Light", size: 17)
        cell.backgroundView = UIImageView(image: UIImage(named: "cell_bg")!)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedValue = reasonsArray[indexPath.row] as? String
        lblReasons.text = selectedValue
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        if tblReasons.hidden == false {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.9)
            tblReasons.hidden = true
            UIView.commitAnimations()
            self.view.addGestureRecognizer(tapGes)
            
        }
    }

    @IBAction func moveBack(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldDidBeginEditing(textField: UITextField) {
        
        //
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textViewDidBeginEditing(textView: UITextView) {
        let height = UIScreen.mainScreen().bounds.height
        if height == 568 || height == 480 {
            scroll.contentOffset = CGPointMake(0, scroll.contentSize.height - txtViewComments.frame.size.height - 200)
        }
    
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        //scroll.contentOffset = CGPointMake(0,0)
        return true
    }
    
    @IBAction func btnReasonsAction(sender: UIButton) {
        createDropDown()
    }
    
    @IBAction func btnPatientBirthDateAction(sender: UIButton) {
        patientBirthDatePicker.hidden = false
        
    }
    
    @IBAction func btnContinueAction(sender: UIButton) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let requestDetailvc = storyBoard.instantiateViewControllerWithIdentifier("RequestDetailVC") as! RequestDetailViewController
        
        requestDetailvc.DestName = txtDestName.text
        requestDetailvc.DestAddress = ""
        requestDetailvc.PatientRoom = txtPatientRoom.text
        requestDetailvc.MemberId = txtPatientMemberId.text
        requestDetailvc.Insurance = selectetInsurance
        
        requestDetailvc.PickUpInfo = pickUpName
        requestDetailvc.RequesterName = txtRequesterName.text
        requestDetailvc.ServiceTyoe = selectetServiceType
        requestDetailvc.Reason = lblReasons.text
        requestDetailvc.Comments = txtViewComments.text

        requestDetailvc.FirstName = txtPatientFirstName.text
        requestDetailvc.LastName = txtPatientLastName.text
        requestDetailvc.facilityName = pickUpName
        requestDetailvc.pickUpaddress = PickUpAddress
        requestDetailvc.pickUpcity = PickUpCity
        requestDetailvc.pickUpstate = PickUpState
        requestDetailvc.pickUpzip = PickUpZip
        requestDetailvc.pickUpphoneNo = ""
        var BOD: String! = String()
        BOD = lblpatientBirthMonth.text! + " , " + lblpatientBirthDay.text! + " " + lblpatientBirthYear.text!
        requestDetailvc.DOB = BOD
        self.navigationController?.pushViewController(requestDetailvc, animated: true)
    }
    
    
    
    func createDropDown() {
    
        if tblReasons.hidden == true {
            self.view.removeGestureRecognizer(tapGes)
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.9)
            tblReasons.hidden = false
            UIView.commitAnimations()
    
        }
    }
    
    @IBAction func btnServiceTypeAction(sender: UIButton) {
        selectetServiceType = serviceTypeArray[sender.tag] as? String
        switch sender.tag
        {
        case 0:
             btnGurney.selected = true
             btnBLS.selected = false
             btnALS.selected = false
             btnCCT.selected = false
            break
        case 1:
            btnGurney.selected = false
            btnBLS.selected = true
            btnALS.selected = false
            btnCCT.selected = false
            break
        case 2:
            btnGurney.selected = false
            btnBLS.selected = false
            btnALS.selected = true
            btnCCT.selected = false
            break
        case 3:
            btnGurney.selected = false
            btnBLS.selected = false
            btnALS.selected = false
            btnCCT.selected = true
            break
        default:
            break
            
        }
    }
    
    @IBAction func btnInsuranceAction(sender: UIButton) {
        selectetInsurance = insuranceArray[sender.tag] as? String
        switch sender.tag
        {
        case 0:
            btnOxford.selected = true
            btnUited.selected = false
            btnBlueCross.selected = false
            btnMedical.selected = false
            btnMedicare.selected = false
            btnLACaries.selected = false
            btnPrivate.selected = false
            break
        case 1:
            btnOxford.selected = false
            btnUited.selected = true
            btnBlueCross.selected = false
            btnMedical.selected = false
            btnMedicare.selected = false
            btnLACaries.selected = false
            btnPrivate.selected = false
            break
        case 2:
            btnOxford.selected = false
            btnUited.selected = false
            btnBlueCross.selected = true
            btnMedical.selected = false
            btnMedicare.selected = false
            btnLACaries.selected = false
            btnPrivate.selected = false
            break
        case 3:
            btnOxford.selected = false
            btnUited.selected = false
            btnBlueCross.selected = false
            btnMedical.selected = true
            btnMedicare.selected = false
            btnLACaries.selected = false
            btnPrivate.selected = false
            break
        case 4:
            btnOxford.selected = false
            btnUited.selected = false
            btnBlueCross.selected = false
            btnMedical.selected = false
            btnMedicare.selected = true
            btnLACaries.selected = false
            btnPrivate.selected = false
            break
        case 5:
            btnOxford.selected = false
            btnUited.selected = false
            btnBlueCross.selected = false
            btnMedical.selected = false
            btnMedicare.selected = false
            btnLACaries.selected = true
            btnPrivate.selected = false
            break
        case 6:
            btnOxford.selected = false
            btnUited.selected = false
            btnBlueCross.selected = false
            btnMedical.selected = false
            btnMedicare.selected = false
            btnLACaries.selected = false
            btnPrivate.selected = true
            break
        default:
            break
            
        }
    }
    

}
