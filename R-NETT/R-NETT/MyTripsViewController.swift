//
//  MyTripsViewController.swift
//  R-NETT
//
//  Created by Mac103 on 10/27/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import UIKit
import CoreData

class MyTripsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblMain: UITableView!
    //let nameArray: NSArray = ["Oxford","Uited","Blue Cross Blue Shields","Medical","Medicare","LA Caries","Private"]
    var filtersection: Int = 0
    var willCall: Bool = false
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var patientInfoResult = [PatientInfo]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMain.tableFooterView  = UIView(frame: CGRectZero)
        getPatientInfo()
        // Do any additional setup after loading the view.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getPatientInfo()
    {
        var error: NSError?
        let fetchRequest = NSFetchRequest(entityName: "PatientInfo")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [PatientInfo]
        if let fetchResultss = fetchResults{
            patientInfoResult = fetchResultss
            tblMain.reloadData()
        }
        else
        {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if filtersection == 0 && willCall == false {
            return 3
        }
        else {
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let patientObj = patientInfoResult[indexPath.row]
        if filtersection == 0 {
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCellWithIdentifier("AllTripsCell", forIndexPath: indexPath) as! AllTripsCustomCell
                cell.lblName.text = patientObj.firstName + " " + patientObj.lastName
                cell.btnSwitch.on = willCall
                cell.lblDate.text = "17/08/2015 | 5:20 PM"
                return cell
            }
            else if indexPath.section == 1  {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("TripsHistoryCell", forIndexPath: indexPath) as! TripsHistoryCustomCell
                cell.lblName.text = patientObj.firstName + " " + patientObj.lastName
                cell.lblDate.text = "17/08/2015 | 5:20 PM"
                return cell
            }
            else  {
                let cell = tableView.dequeueReusableCellWithIdentifier("ActiveTripsCell", forIndexPath: indexPath) as! ActiveTripsCustomCell
                cell.lblName.text = patientObj.firstName + " " + patientObj.lastName
                cell.lblDate.text = "17/08/2015 | 5:20 PM"
                return cell
            
            }
           
        }
        else if filtersection == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("TripsHistoryCell", forIndexPath: indexPath) as! TripsHistoryCustomCell
            cell.lblName.text = patientObj.firstName + " " + patientObj.lastName
            cell.lblDate.text = "17/08/2015 | 5:20 PM"
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("ActiveTripsCell", forIndexPath: indexPath) as! ActiveTripsCustomCell
            cell.lblName.text = patientObj.firstName + " " + patientObj.lastName
            cell.lblDate.text = "17/08/2015 | 5:20 PM"
            return cell
        }
        
    }

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 && filtersection == 0{
            let view:UIView = UIView()
            view.frame = CGRectMake(0, 0, self.view.frame.size.width, 44)
            view.backgroundColor = UIColor.blackColor()
            let label: UILabel = UILabel();
            label.frame = CGRectMake(8, 0, self.view.frame.size.width, 21)
            label.textAlignment = NSTextAlignment.Left
            label.text = "Pending"
            label.font = UIFont(name: "Helvetica Neue", size: 15)
            label.textColor = UIColor.whiteColor()
            view.addSubview(label)
            return view
        }
        else if section == 1 && filtersection == 1 {
            let view:UIView = UIView()
            view.frame = CGRectMake(0, 0, self.view.frame.size.width, 44)
            view.backgroundColor = UIColor.blackColor()
            let label: UILabel = UILabel();
            label.frame = CGRectMake(8, 0, self.view.frame.size.width, 21)
            label.textAlignment = NSTextAlignment.Left
            label.text = "Will Call"
            label.font = UIFont(name: "Helvetica Neue", size: 15)
            label.textColor = UIColor.whiteColor()
            view.addSubview(label)
            return view
        }
        else
        {
            let view:UIView = UIView()
            view.frame = CGRectMake(0, 0, self.view.frame.size.width, 44)
            view.backgroundColor = UIColor.blackColor()
            let label: UILabel = UILabel();
            label.frame = CGRectMake(8, 0, self.view.frame.size.width, 21)
            label.textAlignment = NSTextAlignment.Left
            label.text = "Today"
            label.font = UIFont(name: "Helvetica Neue", size: 15)
            label.textColor = UIColor.whiteColor()
            view.addSubview(label)
            return view
        }

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filtersection == 0 {
            if section == 0 {
                return patientInfoResult.count
            }
            else if section == 1
            {
                return patientInfoResult.count
            }
            else
            {
                return patientInfoResult.count
            }
        }
        else  {
            return patientInfoResult.count
            
        }
    }
    
    @IBAction func btnFilterAction(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            willCall = false
            filtersection = 0
            reloadTableData()
            break
        case 1:
            filtersection = 1
            reloadTableData()
            break
        case 2:
            filtersection = 2
            reloadTableData()
            break
        case 3:
            filtersection = 0
            willCall = true
            reloadTableData()
            break
        default:
            break
            
        }
    }
    
    func reloadTableData() {
        
        tblMain.reloadData()
    }
    
    @IBAction func btnBackAction(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }

}

class AllTripsCustomCell:UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnSwitch: UISwitch!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class TripsHistoryCustomCell:UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class ActiveTripsCustomCell:UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblActive: UILabel!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
