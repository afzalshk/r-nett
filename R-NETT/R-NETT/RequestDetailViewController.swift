//
//  RequestDetailViewController.swift
//  R-NETT
//
//  Created by Mac103 on 10/16/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import UIKit
import CoreData

class RequestDetailViewController: UIViewController {

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var pickupVW: UIView!
    @IBOutlet weak var patientVw: UIView!
    @IBOutlet weak var destinationVW: UIView!
     @IBOutlet weak var mainVW: UIView!
    
    @IBOutlet weak var lblDestName: UILabel!
    @IBOutlet weak var lblDestAddress: UILabel!
    @IBOutlet weak var lblPatientRoom: UILabel!
    @IBOutlet weak var lblMemberId: UILabel!
    @IBOutlet weak var lblInsurance: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblPickUpInfo: UILabel!
    @IBOutlet weak var lblRequesterName: UILabel!
    @IBOutlet weak var lblRequestDate: UILabel!
    @IBOutlet weak var lblServiceTyoe: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var destinationInfoResult = [DestinationInfo]()
    var patientInfoResult = [PatientInfo]()
    var reasonsInfoResult = [Reasons]()
    
    var DestName: String?
    var DestAddress: String?
    var PatientRoom: String?
    var MemberId: String?
    var Insurance: String?
    var DOB: String?
    var FirstName: String?
    var LastName: String?
    var PickUpInfo: String?
    var RequesterName: String?
    var RequestDate: String?
    var ServiceTyoe: String?
    var Reason: String?
    var Comments: String?
    
    var facilityName: String?
    var pickUpaddress: String?
    var pickUpcity: String?
    var pickUpstate: String?
    var pickUpzip: String?
    var pickUpphoneNo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        fetchRecords()
        
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setViews()
    {
        btnEdit.layer.cornerRadius = 20
        btnEdit.clipsToBounds = true
        btnEdit.layer.borderWidth = 0.5
        btnEdit.layer.borderColor = UIColor.whiteColor().CGColor
        btnSubmit.layer.cornerRadius = 20
        btnSubmit.clipsToBounds = true
        btnSubmit.layer.borderWidth = 0.5
        btnSubmit.layer.borderColor = UIColor.whiteColor().CGColor
        mainScroll.userInteractionEnabled = true
        let height = UIScreen.mainScreen().bounds.height
        if height == 667 {
            //mainScroll.frame = CGRectMake(0,50,375,702)
            //mainScroll.contentSize = CGSizeMake(375, 1000)
        }
        else if height == 736 {
            //mainScroll.frame = CGRectMake(0,50,414,702)
             //mainScroll.contentSize = CGSizeMake(414, 1000)
        }
        else {
            //mainScroll.frame = CGRectMake(0,50,320,702)
             //mainScroll.contentSize = CGSizeMake(320, 1000)
        }
       
        
    }
    
    func fetchRecords() {
        
        getPickUpInfo()
        getPatientInfo()
        getDestinationInfo()
    }
    
    func getPickUpInfo()
    {
        lblPickUpInfo.text = facilityName! + "," + pickUpaddress! + "," + pickUpcity! + "," + pickUpstate! + "," +  pickUpzip! + "," +  pickUpphoneNo!
        lblRequesterName.text = RequesterName
        lblRequestDate.text = RequestDate
        
    }
    
    func getPatientInfo()
    {
        lblMemberId.text = MemberId
        lblInsurance.text = Insurance
        lblDOB.text = DOB
        lblFirstName.text = FirstName! + " " + LastName!
        lblPatientRoom.text = PatientRoom
        
    }
    
    func getDestinationInfo()
    {
        lblDestName.text = DestName
        lblReason.text = Reason
        lblComments.text = Comments
        
       //lblDestAddress2.text = destinationInfoObj.address1 + "," + destinationInfoObj.address2 + "," + destinationInfoObj.city + "," + destinationInfoObj.state + "," + destinationInfoObj.zip + "," + destinationInfoObj.phoneNo
    }
    
    func addPickUpInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("PickUpInfo", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        
        myMO.setValue(facilityName, forKey: "facilityName")
        myMO.setValue(pickUpaddress, forKey: "address")
        myMO.setValue(pickUpcity, forKey: "city")
        myMO.setValue(pickUpstate, forKey: "state")
        myMO.setValue(pickUpzip, forKey: "zip")
        myMO.setValue(pickUpphoneNo, forKey: "phoneNo")
        myMO.setValue(RequesterName, forKey: "requesterName")
        myMO.setValue(RequestDate, forKey: "requestDate")
        myMO.setValue("05", forKey: "estimatedPickTime")
        myMO.setValue(ServiceTyoe, forKey: "serviceType")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
        
    }
    
    func addPatientInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("PatientInfo", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(FirstName, forKey: "firstName")
        myMO.setValue(LastName, forKey: "lastName")
        myMO.setValue(Insurance, forKey: "insurance")
        
        myMO.setValue(DOB, forKey: "birthDate")
        myMO.setValue(MemberId, forKey: "memberId")
        myMO.setValue(PatientRoom, forKey: "roomNo")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
    }
    
    func addDestinationInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("DestinationInfo", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(DestName, forKey: "destinationName")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
    }
    
    func addReasonInfo()
    {
        var myMO: NSManagedObject
        myMO = NSEntityDescription.insertNewObjectForEntityForName("Reasons", inManagedObjectContext: managedObjectContext!) as! NSManagedObject
        myMO.setValue(Reason, forKey: "reasonDesc")
        myMO.setValue(NSDate(), forKey: "date")
        var error: NSError?
        if !managedObjectContext!.save(&error){
            println("Could not save \(error), \(error?.userInfo)")
        }
        navigationController?.popToRootViewControllerAnimated(true)
        
    }

    @IBAction func btnBackAction(sender: UIButton) {
        
        navigationController?.popViewControllerAnimated(true)
        
    }
    
    @IBAction func btnSubmitAction(sender: AnyObject) {
        addPickUpInfo()
        addPatientInfo()
        addDestinationInfo()
        addReasonInfo()
        
    }
    
    @IBAction func btnEditAction(sender: AnyObject) {
        
        navigationController?.popViewControllerAnimated(true)
    }
}
