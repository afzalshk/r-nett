//
//  MyRequestsViewController.swift
//  R-NETT
//
//  Created by Mac103 on 10/17/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import UIKit
import CoreData
class MyRequestsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblView: UITableView!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var pickUpInfoResult = [PickUpInfo]()
    var destinationInfoResult = [DestinationInfo]()
    var patientInfoResult = [PatientInfo]()
    var reasonsInfoResult = [Reasons]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.dataSource = self
        tblView.delegate = self
        fetchRecords()
        // Do any additional setup after loading the view.
    }
    
    func fetchRecords() {
        getPickUpInfo()
        //getPatientInfo()
        getDestinationInfo()
        tblView.reloadData()
    }
    
    func getPickUpInfo()
    {
        var error: NSError?
        let fetchRequest = NSFetchRequest(entityName: "PickUpInfo")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [PickUpInfo]
        if let fetchResultss = fetchResults{
            pickUpInfoResult = fetchResultss
        }
        else
        {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    func getPatientInfo()
    {
        
        var error: NSError?
        let fetchRequest = NSFetchRequest(entityName: "PatientInfo")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [PatientInfo]
        if let fetchResultss = fetchResults{
            patientInfoResult = fetchResultss
        }
        else
        {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    func getDestinationInfo()
    {
        var error: NSError?
        let fetchRequest = NSFetchRequest(entityName: "DestinationInfo")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        let fetchResults = managedObjectContext!.executeFetchRequest(fetchRequest, error: &error) as? [DestinationInfo]
        if let fetchResultss = fetchResults{
            destinationInfoResult = fetchResultss
        }
        else
        {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(tableView: UITableView, HeightForHeaderInSection section: Int) -> CGFloat {
//        
//        return 60
//    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if section == 0 {
//            let view:UIView = UIView()
//            view.frame = CGRectMake(0, 0, self.view.frame.size.width, 60)
//            view.backgroundColor = UIColor.lightGrayColor()
//            let label: UILabel = UILabel();
//            label.frame = CGRectMake(0, 0, self.view.frame.size.width, 21)
//            label.textAlignment = NSTextAlignment.Center
//            label.text = "Pick Up Info"
//            label.textColor = UIColor.whiteColor()
//            view.addSubview(label)
//            return view
//        }
//        else if section == 1 {
//            let view:UIView = UIView()
//            view.frame = CGRectMake(0, 0, self.view.frame.size.width, 60)
//            view.backgroundColor = UIColor.lightGrayColor()
//            let label: UILabel = UILabel();
//            label.frame = CGRectMake(0, 0, self.view.frame.size.width, 21)
//            label.textAlignment = NSTextAlignment.Center
//            label.text = "Patient Info"
//            label.textColor = UIColor.whiteColor()
//            view.addSubview(label)
//            return view
//        }
//        else
//        {
//            let view:UIView = UIView()
//            view.frame = CGRectMake(0, 0, self.view.frame.size.width, 60)
//            view.backgroundColor = UIColor.lightGrayColor()
//            let label: UILabel = UILabel();
//            label.frame = CGRectMake(0, 0, self.view.frame.size.width, 21)
//            label.textAlignment = NSTextAlignment.Center
//            label.text = "Destination Info"
//            label.textColor = UIColor.whiteColor()
//            view.addSubview(label)
//            return view
//        }
//        
//    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pickUpInfoResult.count
//        if section == 0
//        {
//            return pickUpInfoResult.count
//        }
//        else if section == 1
//        {
//            return patientInfoResult.count
//        }
//        else
//        {
//           return destinationInfoResult.count
//        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let pickUpObj = pickUpInfoResult[indexPath.row]
        let destinationObj = destinationInfoResult[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("pickupCell", forIndexPath: indexPath) as! PickUpCustomCell
        cell.lblFromInfo.text = pickUpObj.facilityName + "," + pickUpObj.address + "," + pickUpObj.city + "," + pickUpObj.state + "," +  pickUpObj.zip + "," +  pickUpObj.phoneNo

        cell.lblToInfo.text = destinationObj.destinationName + "," + destinationObj.address1 + "," + destinationObj.address2 + "," + destinationObj.city + "," + destinationObj.state + "," + destinationObj.zip + "," + destinationObj.phoneNo
        cell.backgroundColor = UIColor.clearColor()
        return cell
//        if indexPath.section == 0
//        {
//            let pickUpObj = pickUpInfoResult[indexPath.row]
//            let cell = tableView.dequeueReusableCellWithIdentifier("pickupCell", forIndexPath: indexPath) as! PickUpCustomCell
//            cell.lblPickUpInfo.text = pickUpObj.facilityName + "," + pickUpObj.address + "," + pickUpObj.city + "," + pickUpObj.state + "," +  pickUpObj.zip + "," +  pickUpObj.phoneNo
//            cell.lblRequesterName.text = pickUpObj.requesterName
//            cell.lblPickUpTime.text = pickUpObj.estimatedPickTime
//            cell.lblRequesterDate.text = pickUpObj.requestDate
//            cell.backgroundColor = UIColor.clearColor()
//            return cell
//        }
//        else if indexPath.section == 1
//        {
//            let patientObj = patientInfoResult[indexPath.row]
//            let cell = tableView.dequeueReusableCellWithIdentifier("patientcell", forIndexPath: indexPath) as! PatientInfoCustomCell
//            cell.lblFirstName.text = patientObj.firstName
//            cell.lblLastName.text = patientObj.lastName
//            cell.lblDOB.text = patientObj.birthDate
//            cell.lblInsurance.text = patientObj.insurance
//            cell.lblMemberId.text = patientObj.memberId
//            cell.lblRoomNo.text = patientObj.roomNo
//            cell.backgroundColor = UIColor.clearColor()
//            return cell
//        }
//        else
//        {
//            let destinationObj = destinationInfoResult[indexPath.row]
//            let cell = tableView.dequeueReusableCellWithIdentifier("destinationcell", forIndexPath: indexPath) as! DestinationCustomCell
//            
//            cell.lblDestName.text = destinationObj.destinationName
//            cell.lblAddress1.text = destinationObj.address1 + "," + destinationObj.city + "," + destinationObj.state + "," + destinationObj.zip + "," + destinationObj.phoneNo
//            cell.lblAddress2.text = destinationObj.address2 + "," + destinationObj.city + "," + destinationObj.state + "," + destinationObj.zip + "," + destinationObj.phoneNo
//            cell.backgroundColor = UIColor.clearColor()
//            return cell
//        }
    }
    
    
    
    @IBAction func btnBackAction(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}

class PickUpCustomCell:UITableViewCell {
    
    @IBOutlet weak var lblToInfo: UILabel!
    @IBOutlet weak var lblFromInfo: UILabel!
//    @IBOutlet weak var lblPickUpTime: UILabel!
//    @IBOutlet weak var lblRequesterDate: UILabel!
//    @IBOutlet weak var lblRequesterName: UILabel!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

//class PatientInfoCustomCell:UITableViewCell {
//    
//    @IBOutlet weak var lblFirstName: UILabel!
//    @IBOutlet weak var lblLastName: UILabel!
//    @IBOutlet weak var lblDOB: UILabel!
//    @IBOutlet weak var lblInsurance: UILabel!
//    @IBOutlet weak var lblMemberId: UILabel!
//    @IBOutlet weak var lblRoomNo: UILabel!
//    
//    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//    }
//    
//    required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//    }
//    
//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//    }
//    
//}
//
//class DestinationCustomCell:UITableViewCell {
//    
//    
//    @IBOutlet weak var lblDestName: UILabel!
//    @IBOutlet weak var lblAddress1: UILabel!
//    @IBOutlet weak var lblAddress2: UILabel!
//    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//    }
//    
//    required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//    }
//    
//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//    }
//    
//}
