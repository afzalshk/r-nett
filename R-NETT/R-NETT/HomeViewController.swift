//
//  HomeViewController.swift
//  R-NETT
//
//  Created by Mac103 on 10/12/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import UIKit
import MapKit
class HomeViewController: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate{

    @IBOutlet weak var lblServices: UILabel!
    @IBOutlet weak var lblServiceType: UILabel!
    @IBOutlet weak var imgVwHdVan: UIImageView!
    @IBOutlet weak var imgVwVain: UIImageView!
    @IBOutlet weak var servicesScrollVw: UIScrollView!
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var lblEAT: UILabel!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var btnMyRequests: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnWillCall: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    let regionRadius: CLLocationDistance = 1000
    let googleAPIKey: String = "AIzaSyCWZO7k3RpDc4SVMYCrmEoDtVlEAjXhdas"
    let searchType: String = "taxi_stand"
    var destinationLocation = CLLocationCoordinate2D(latitude: 37.768314,longitude: -122.434768)
    let currentLocation = CLLocationCoordinate2D(latitude: 37.771906,longitude: -122.433505)
    let destinationLoc = CLLocationCoordinate2D(latitude: 37.768314,longitude: -122.434768)
    let span = MKCoordinateSpanMake(0.015, 0.015)
    var currenttimeETA : NSTimeInterval = 0
    var currentETA : Int = 0
    
    let latArray: NSArray = ["37.771199","37.772169","37.774311","37.775110"]
    let longArray: NSArray = ["-122.430290","-122.437174","-122.435947","-122.429322"]
    let moveArray  = [CLLocationCoordinate2D(latitude: 37.770554,longitude: -122.435172),CLLocationCoordinate2D(latitude: 37.779589,longitude: -122.431960), CLLocationCoordinate2D(latitude: 37.772534,longitude: -122.427164), CLLocationCoordinate2D(latitude: 37.773544,longitude: -122.426154)]
    var timeETAs = [0.0,0.0,0.0,0.0]
    var firstTime: Bool = true
    var isSubView: Bool = false
    let tapRec = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        let span = MKCoordinateSpanMake(0.015, 0.015)
        let region = MKCoordinateRegion(center: currentLocation, span: span)
        mapView.setRegion(region, animated: true)
        let annotation = CustomPointAnnotation()
        annotation.imageName = "self"
        
        annotation.coordinate = currentLocation
        mapView.addAnnotation(annotation)
        let overlayRadius: CLLocationDistance = 0
        mapView.addOverlay(MKCircle(centerCoordinate: currentLocation, radius: overlayRadius))
        tapRec.addTarget(self, action: "goToRequest")
        requestView.userInteractionEnabled = true
        requestView.addGestureRecognizer(tapRec)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if firstTime == true {
            firstTime = false
            mapAndannotation()
        }
        else
        {
            
            makeOverlayPath()
            let annotationsToRemove = mapView.annotations.filter { $0 !== self.mapView.userLocation }
            mapView.removeAnnotations( annotationsToRemove )
            addAnnovationAfterRequest()
        }
        
        
        
    }
    override func viewDidLayoutSubviews() {
        if isSubView == false {
           isSubView = true
        self.imgVwHdVan.frame  = CGRectMake(CGRectGetMaxX(self.servicesScrollVw.frame) - 80, self.imgVwHdVan.frame.origin.y, self.imgVwHdVan.frame.size.width,self.imgVwHdVan.frame.size.height)
        self.imgVwVain.frame = CGRectMake(self.servicesScrollVw.frame.size.width/2 - self.imgVwVain.frame.size.width/2, self.imgVwVain.frame.origin.y, self.imgVwVain.frame.size.width, self.imgVwVain.frame.size.height)
        }
    }
    
    
    func goToRequest()
    {
        getReverseGeoCoding(false)
        //var requestVC = MakeRequestViewController(nibName: "MakeRequestViewController", bundle: nil)
        //self.navigationController?.pushViewController(requestVC, animated: true)
    }
    
    func mapAndannotation ()
    {
        
        for var i = 0 ; i < latArray.count; i++
        {
            let annotation = CustomPointAnnotation()
            annotation.imageName = "taxi_icon"
            let currentlatitude = NSNumberFormatter().numberFromString(latArray.objectAtIndex(i) as! String)!.doubleValue
            let curerentlangitude = NSNumberFormatter().numberFromString(longArray.objectAtIndex(i) as! String)!.doubleValue
            
            let latitude: CLLocationDegrees =  CLLocationDegrees(currentlatitude as NSNumber)
            let longitude: CLLocationDegrees = CLLocationDegrees(curerentlangitude as NSNumber)
            let initialLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude,
                longitude: longitude)
            
            let center:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:latitude  , longitude: longitude)
            let region: MKCoordinateRegion = MKCoordinateRegion(center: center, span: span)
            
            //self.mapView.setRegion(region, animated: true)
            annotation.coordinate = initialLocation
            self.mapView.addAnnotation(annotation) //drops the pin
            
            let index = i
            let latestETA = self.getDirectonObject(initialLocation, destLoc: self.currentLocation)
            latestETA.calculateETAWithCompletionHandler({
                (response: MKETAResponse?, error: NSError?) in
                if response != nil {
                    var routeSeconds = response!.expectedTravelTime
                    self.timeETAs[index] = routeSeconds
                    if (index == 0) {
                        self.lblEAT.text = self.stringFromTimeInterval(routeSeconds) as String
                    } else if (index == 1 && self.timeETAs[index] > self.timeETAs[index]) {
                        self.lblEAT.text = self.stringFromTimeInterval(routeSeconds) as String
                    } else {
                        if (index == 2) {
                            self.timeETAs[index + 1] = routeSeconds
                        }
                        let numMin = minElement(self.timeETAs)
                        self.lblEAT.text = self.stringFromTimeInterval(numMin) as String
                    }
                }
            })
            
            makeMovePath(initialLocation, destLoc: moveArray[i], anno : annotation, vehicleIndex : i)
            
        }
    }
    
    func makeMovePath(initialLocation : CLLocationCoordinate2D, destLoc : CLLocationCoordinate2D, anno : CustomPointAnnotation, vehicleIndex : Int){
        
        let directions = getDirectonObject(initialLocation, destLoc: destLoc)
        
        directions.calculateDirectionsWithCompletionHandler ({
            (response: MKDirectionsResponse?, error: NSError?) in
            
            if error == nil {
                for route in response!.routes as! [MKRoute]{
                    let coor = route.polyline.coordinate
                    var coordsPointer = UnsafeMutablePointer<CLLocationCoordinate2D>.alloc(route.polyline.pointCount)
                    route.polyline.getCoordinates(coordsPointer, range: NSMakeRange(0, route.polyline.pointCount))
                    var coords: [CLLocationCoordinate2D] = []
                    coords.reserveCapacity(route.polyline.pointCount)
                    for i in 0..<route.polyline.pointCount {
                        coords.append(coordsPointer[i])
                    }
                    coordsPointer.dealloc(route.polyline.pointCount)
                    var x : Double = 6;
                    for i in 0..<route.polyline.pointCount {
                        let indexCoordinate = coords[i]
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(x * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {
                            let latestETA = self.getDirectonObject(indexCoordinate, destLoc: self.currentLocation)
                            latestETA.calculateETAWithCompletionHandler({
                                (response: MKETAResponse?, error: NSError?) in
                                if response != nil {
                                    var routeSeconds = response!.expectedTravelTime
                                    self.timeETAs[vehicleIndex] = routeSeconds
                                    println(self.timeETAs)
                                    let numMin = minElement(self.timeETAs)
                                    self.lblEAT.text = self.stringFromTimeInterval(numMin) as String
                                    anno.coordinate = indexCoordinate
                                }
                            })
                        }
                        
                        x = x+1
                    }
                }
            }
            else{
                
                println("trace the error \(error?.localizedDescription)")
                
            }
        })
        
    }
    
    func makeMovePath(initialLocation : CLLocationCoordinate2D, destLoc : CLLocationCoordinate2D, anno : CustomPointAnnotation){
        
        let directions = getDirectonObject(initialLocation, destLoc: destLoc)
        
        directions.calculateDirectionsWithCompletionHandler ({
            (response: MKDirectionsResponse?, error: NSError?) in
            
            if error == nil {
                for route in response!.routes as! [MKRoute]{
                    let coor = route.polyline.coordinate
                    var coordsPointer = UnsafeMutablePointer<CLLocationCoordinate2D>.alloc(route.polyline.pointCount)
                    route.polyline.getCoordinates(coordsPointer, range: NSMakeRange(0, route.polyline.pointCount))
                    var coords: [CLLocationCoordinate2D] = []
                    coords.reserveCapacity(route.polyline.pointCount)
                    for i in 0..<route.polyline.pointCount {
                        coords.append(coordsPointer[i])
                    }
                    coordsPointer.dealloc(route.polyline.pointCount)
                    var x : Double = 3;
                    for i in 0..<route.polyline.pointCount {
                        let indexCoordinate = coords[i]
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(x * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {
                            let latestETA = self.getDirectonObject(indexCoordinate, destLoc: self.currentLocation)
                            latestETA.calculateETAWithCompletionHandler({
                                (response: MKETAResponse?, error: NSError?) in
                                if response != nil {
                                    var routeSeconds = response!.expectedTravelTime
                                    //let numMin = minElement(self.timeETAs)
                                    self.lblEAT.text = self.stringFromTimeInterval(routeSeconds) as String
                                    anno.coordinate = indexCoordinate
                                }
                            })
                        }
                        
                        x = x+1
                    }
                    //                   let overLays =  self.mapView.overlays as? [MKOverlay]
                    //                    for  ol:MKOverlay in overLays {
                    //                        if ol is MKPolyline {
                    //                    }
                    //self.mapView.removeOverlay()
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(x * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        self.makeOverlayAfter()
                        self.makeMoveAfter(self.currentLocation, destLoc: self.destinationLoc, anno: anno)
                    }
                }
            }
            else{
                
                println("trace the error \(error?.localizedDescription)")
                
            }
        })
        
    }
    
    func makeMoveAfter(initialLocation : CLLocationCoordinate2D, destLoc : CLLocationCoordinate2D, anno : CustomPointAnnotation){
        
        let directions = getDirectonObject(initialLocation, destLoc: destLoc)
        
        directions.calculateDirectionsWithCompletionHandler ({
            (response: MKDirectionsResponse?, error: NSError?) in
            
            if error == nil {
                for route in response!.routes as! [MKRoute]{
                    let coor = route.polyline.coordinate
                    var coordsPointer = UnsafeMutablePointer<CLLocationCoordinate2D>.alloc(route.polyline.pointCount)
                    route.polyline.getCoordinates(coordsPointer, range: NSMakeRange(0, route.polyline.pointCount))
                    var coords: [CLLocationCoordinate2D] = []
                    coords.reserveCapacity(route.polyline.pointCount)
                    for i in 0..<route.polyline.pointCount {
                        coords.append(coordsPointer[i])
                    }
                    coordsPointer.dealloc(route.polyline.pointCount)
                    var x : Double = 2;
                    for i in 0..<route.polyline.pointCount {
                        let indexCoordinate = coords[i]
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(x * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {
                            let latestETA = self.getDirectonObject(indexCoordinate, destLoc: self.destinationLocation)
                            latestETA.calculateETAWithCompletionHandler({
                                (response: MKETAResponse?, error: NSError?) in
                                if response != nil {
                                    var routeSeconds = response!.expectedTravelTime
                                    //let numMin = minElement(self.timeETAs)
                                    self.lblEAT.text = self.stringFromTimeInterval(routeSeconds) as String
                                    anno.coordinate = indexCoordinate
                                }
                            })
                        }
                        
                        x = x+1
                    }
                }
            }
            else{
                
                println("trace the error \(error?.localizedDescription)")
                
            }
        })
        
    }
    
    func makeOverlayAfter(){
        
        //let currentlatitude = NSNumberFormatter().numberFromString(latArray.objectAtIndex(0) as! String)!.doubleValue
        //let curerentlangitude = NSNumberFormatter().numberFromString(longArray.objectAtIndex(0) as! String)!.doubleValue
        //let latitude: CLLocationDegrees =  CLLocationDegrees(currentlatitude as NSNumber)
        //let longitude: CLLocationDegrees = CLLocationDegrees(curerentlangitude as NSNumber)
        //CLLocationCoordinate2D(latitude: moveArray.objectAtIndex(0),longitude: longitude)
        
        //let initialLocation: CLLocationCoordinate2D = moveArray[0]
        let directions = getDirectonObject(currentLocation, destLoc: destinationLoc)
        
        directions.calculateDirectionsWithCompletionHandler ({
            (response: MKDirectionsResponse?, error: NSError?) in
            
            if error == nil {
                for route in response!.routes as! [MKRoute]{
                    self.mapView.addOverlay(route.polyline, level: MKOverlayLevel.AboveRoads)
                    let routeDistance = route.distance
                    var routeSeconds = route.expectedTravelTime
                    if self.currenttimeETA == 0 {
                        self.currenttimeETA = routeSeconds
                    } else if (routeSeconds < self.currenttimeETA){
                        self.currenttimeETA = routeSeconds
                    }
                    
                    self.lblEAT.text = self.stringFromTimeInterval(self.currenttimeETA) as String
                    //println("distance between two points is \(routeSeconds) and \(routeDistance)")
                }
            }
            else{
                
                println("trace the error \(error?.localizedDescription)")
                
            }
        })
        
        
        
    }

    
    func getDirectonObject (initialLocation : CLLocationCoordinate2D, destLoc : CLLocationCoordinate2D) ->MKDirections {
        var sourcePlacemark = MKPlacemark(coordinate: initialLocation, addressDictionary: nil)
        var source = MKMapItem(placemark: sourcePlacemark)
        
        var desitnationPlacemark = MKPlacemark(coordinate: destLoc, addressDictionary: nil)
        var destination = MKMapItem(placemark: desitnationPlacemark)
        
        let request:MKDirectionsRequest = MKDirectionsRequest()
        request.setSource(source)
        request.setDestination(destination)
        
        // Specify the transportation type
        request.transportType = MKDirectionsTransportType.Automobile;
        
        // If you're open to getting more than one route,
        // requestsAlternateRoutes = true; else requestsAlternateRoutes = false;
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        return directions
    }
    
    let calloutAnnotationViewIdentifier = "CalloutAnnotation"
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKAnnotation {
            var pin = mapView.dequeueReusableAnnotationViewWithIdentifier(calloutAnnotationViewIdentifier)
            if pin == nil {
                if let anno = annotation as? CustomPointAnnotation {
                    if anno.imageName == "self" {
                            pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier:
                            calloutAnnotationViewIdentifier)
                            //pin.image = UIImage(named: "circle_icon")
                        
                        
                    } else {
                        pin = MKAnnotationView(annotation: annotation, reuseIdentifier:
                            calloutAnnotationViewIdentifier)
                        pin!.image = UIImage(named:"taxi_icon")
                    }
                }
                pin!.canShowCallout = false
            }
            else
            {
                if let anno = annotation as? CustomPointAnnotation {
                    if (anno.imageName == "self" && (pin.annotation as? CustomPointAnnotation)?.imageName == "self") ||
                        (anno.imageName == "taxi_icon" && (pin.annotation as? CustomPointAnnotation)?.imageName == "taxi_icon") {
                            pin!.annotation = annotation
                    } else {
                        if anno.imageName == "self" {
                            pin = MKPinAnnotationView(annotation: annotation, reuseIdentifier:
                                calloutAnnotationViewIdentifier)
                        } else {
                            pin = MKAnnotationView(annotation: annotation, reuseIdentifier:
                                calloutAnnotationViewIdentifier)
                            pin!.image = UIImage(named:"taxi_icon")
                        }
                        
                    }
                }
            }
            
            
            
            return pin
        }
        return nil
    }

    
func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            var circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.redColor()
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        }
        else if overlay is MKPolyline {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blueColor()
            polylineRenderer.lineWidth = 2
            return polylineRenderer
        }
        else {
            return nil
        }
    }
    
    func addAnnovationAfterRequest()
    {
         var pinLocation : CLLocationCoordinate2D = currentLocation
         var objectAnnotation = CustomPointAnnotation()
         objectAnnotation.coordinate = pinLocation
         objectAnnotation.imageName = "self"
         self.mapView.addAnnotation(objectAnnotation)

        var pinLocation2 : CLLocationCoordinate2D = moveArray[0]
        let objectAnnotation2 = CustomPointAnnotation()
        objectAnnotation2.imageName = "taxi_icon"
        objectAnnotation2.coordinate = pinLocation2
        self.mapView.addAnnotation(objectAnnotation2)
        makeMovePath(moveArray[0], destLoc: currentLocation, anno: objectAnnotation2)

    
    }
        
    func makeOverlayPath(){
        
        //let currentlatitude = NSNumberFormatter().numberFromString(latArray.objectAtIndex(0) as! String)!.doubleValue
        //let curerentlangitude = NSNumberFormatter().numberFromString(longArray.objectAtIndex(0) as! String)!.doubleValue
        //let latitude: CLLocationDegrees =  CLLocationDegrees(currentlatitude as NSNumber)
        //let longitude: CLLocationDegrees = CLLocationDegrees(curerentlangitude as NSNumber)
        //CLLocationCoordinate2D(latitude: moveArray.objectAtIndex(0),longitude: longitude)
        
        let initialLocation: CLLocationCoordinate2D = moveArray[0]
        let directions = getDirectonObject(initialLocation, destLoc: currentLocation)
        
        directions.calculateDirectionsWithCompletionHandler ({
            (response: MKDirectionsResponse?, error: NSError?) in
            
            if error == nil {
                for route in response!.routes as! [MKRoute]{
                    self.mapView.addOverlay(route.polyline, level: MKOverlayLevel.AboveRoads)
                    let routeDistance = route.distance
                    var routeSeconds = route.expectedTravelTime
                    if self.currenttimeETA == 0 {
                        self.currenttimeETA = routeSeconds
                    } else if (routeSeconds < self.currenttimeETA){
                        self.currenttimeETA = routeSeconds
                    }
                    
                    self.lblEAT.text = self.stringFromTimeInterval(self.currenttimeETA) as String
                    //println("distance between two points is \(routeSeconds) and \(routeDistance)")
                }
            }
            else{
                
                println("trace the error \(error?.localizedDescription)")
                
            }
        })
        
    }
    
    func stringFromTimeInterval(interval:NSTimeInterval) -> NSString {
        
        var ti = NSInteger(interval)
        var seconds = ti % 60
        var minutes = (ti / 60) % 60
        var hours = (ti / 3600)
        //return NSString(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
        return NSString(format: "%d Min",minutes)
    }
    
    
    func getReverseGeoCoding(isOn: Bool) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude:currentLocation.latitude, longitude: currentLocation.longitude)
        let requestVC = MakeRequestViewController(nibName: "MakeRequestViewController", bundle: nil)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            let placeArray = placemarks as? [CLPlacemark]
            var placeMark: CLPlacemark!
            placeMark = placeArray?[0]
            
            if let locationName = placeMark.addressDictionary["Name"] as? NSString {
                requestVC.pickUpName = locationName as? String
            }
            
            if let street = placeMark.addressDictionary["Thoroughfare"] as? NSString {
                requestVC.PickUpAddress = street as String
            }
        
            if let city = placeMark.addressDictionary["City"] as? NSString {
                requestVC.PickUpCity = city as String
            }
            
            if let zip = placeMark.addressDictionary["ZIP"] as? NSString {
                requestVC.PickUpZip = zip as String
            }
            if let state = placeMark.addressDictionary["State"] as? NSString {
                requestVC.PickUpState = state as String
            }
            
            // Country
            if let country = placeMark.addressDictionary["Country"] as? NSString {
                //
            }
            requestVC.isSwitch = isOn
            self.navigationController?.pushViewController(requestVC, animated: true)
        })
        
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func queryGooglePlaces(googleType: String)  {
        
        var url: String = "https://maps.googleapis.com/maps/api/place/search/json?location=\(currentLocation.latitude),\(currentLocation.longitude)&radius=\(regionRadius)&types=\(googleType)&sensor=true&key=\(googleAPIKey)"
        
        var googleRequestURL: NSURL = NSURL(string: url)!
        var request1: NSURLRequest = NSURLRequest(URL: googleRequestURL)
        let queue:NSOperationQueue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(request1, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var err: NSError
            var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
            println("AsSynchronous\(jsonResult)")
        })
    }
    
    
    
    // Action Methods
    @IBAction func btnSwitchAction(sender: UISwitch) {
        
        let isOn:Bool = sender.on
        if isOn {
            getReverseGeoCoding(isOn)
        }
    }
    
    @IBAction func btnServiceTypeAction(sender: UIButton) {
        if sender.tag == 0 {
            
            
            UIView.animateWithDuration(0.4, animations: {
                self.lblServiceType.text = "BLS"
                self.lblServices.text = "( BASIC LIFE SUPPORT )"
                self.imgVwHdVan.frame  = CGRectMake(CGRectGetMaxX(self.servicesScrollVw.frame) - 80, self.imgVwHdVan.frame.origin.y, self.imgVwHdVan.frame.size.width,self.imgVwHdVan.frame.size.height)
                
                self.imgVwVain.frame = CGRectMake(self.servicesScrollVw.frame.size.width/2 - self.imgVwVain.frame.size.width/2, self.imgVwVain.frame.origin.y, self.imgVwVain.frame.size.width, self.imgVwVain.frame.size.height)
            })
        }
        else {
            self.lblServiceType.text = "ALS"
            self.lblServices.text = "( ADVANCE LIFE SUPPORT )"
            UIView.animateWithDuration(0.4, animations: {
                self.imgVwVain.frame  = CGRectMake(CGRectGetMinX(self.servicesScrollVw.frame) + 20, self.imgVwVain.frame.origin.y, self.imgVwVain.frame.size.width,self.imgVwVain.frame.size.height)
                
                self.imgVwHdVan.frame = CGRectMake(self.servicesScrollVw.frame.size.width/2 - self.imgVwHdVan.frame.size.width/2 , self.imgVwHdVan.frame.origin.y, self.imgVwHdVan.frame.size.width, self.imgVwHdVan.frame.size.height)
            })
        }
        
    }
    
    @IBAction func btnRequestAction(sender: UIButton) {
       getReverseGeoCoding(false)
    }
    
    @IBAction func btnMyRequestsAction(sender: UIButton) {
        
        let myRequestViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyRequestsVC") as! MyRequestsViewController
        self.navigationController?.pushViewController(myRequestViewController, animated: true)
    }
}


class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
    var currentView: UIView?
}
