//
//  PickUpInfo.swift
//  R-NETT
//
//  Created by Mac103 on 10/14/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import Foundation
import CoreData
@objc(PickUpInfo)
class PickUpInfo: NSManagedObject {

    @NSManaged var pickId: NSNumber
    @NSManaged var facilityName: String
    @NSManaged var address: String
    @NSManaged var city: String
    @NSManaged var state: String
    @NSManaged var zip: String
    @NSManaged var phoneNo: String
    @NSManaged var requesterName: String
    @NSManaged var requestDate: String
    @NSManaged var estimatedPickTime: String
    @NSManaged var serviceType: String
    @NSManaged var date: NSDate

}
