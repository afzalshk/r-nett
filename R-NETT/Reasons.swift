//
//  Reasons.swift
//  R-NETT
//
//  Created by Mac103 on 10/14/15.
//  Copyright (c) 2015 leadconcept. All rights reserved.
//

import Foundation
import CoreData
@objc(Reasons)
class Reasons: NSManagedObject {

    @NSManaged var reasonId: NSNumber
    @NSManaged var reasonDesc: String
    @NSManaged var date: NSDate

}
